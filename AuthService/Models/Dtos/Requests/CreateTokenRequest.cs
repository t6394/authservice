﻿using System;

namespace AuthService.Models.Dtos.Requests
{
    public class CreateTokenRequest
    {
        public Guid CustomerId { get; set; }
        public string Role { get; set; }
    }
}