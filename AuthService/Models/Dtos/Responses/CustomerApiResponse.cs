﻿namespace AuthService.Models.Dtos.Responses
{
    public class CustomerApiResponse
    {
        public bool Data { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
        public int StatusCode { get; set; }

    }
}