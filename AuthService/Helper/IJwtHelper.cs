﻿using System;
using AuthService.Models.Dtos.Responses;


namespace AuthService.Helper
{
    public interface IJwtHelper
    {
        AccessTokenResponse GenereteJwtToken(Guid id, string role);
        TokenHandlerResponse ValidateJwtToken(string token);
    }
}