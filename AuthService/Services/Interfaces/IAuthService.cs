﻿using System.Threading.Tasks;
using AuthService.Models.Dtos.Requests;
using AuthService.Models.Dtos.Responses;
using Core.ServerResponse;

namespace AuthService.Services.Interfaces
{
    public interface IAuthService
    {
        Task<Response<AccessTokenResponse>> CreateToken(CreateTokenRequest request);
        Response<TokenHandlerResponse> ValidateToken(string request);
    }
}