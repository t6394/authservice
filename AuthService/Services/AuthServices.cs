﻿using System.Threading.Tasks;
using AuthService.Clients;
using AuthService.Helper;
using AuthService.Models.Dtos.Requests;
using AuthService.Models.Dtos.Responses;
using AuthService.Services.Interfaces;
using Core.ServerResponse;
using TokenHandlerResponse = AuthService.Models.Dtos.Responses.TokenHandlerResponse;

namespace AuthService.Services
{
    public class AuthServices : IAuthService
    {
        private readonly ICustomerClient _customerClient;
        private readonly IJwtHelper _jwtHelper;

        public AuthServices(ICustomerClient customerClient, IJwtHelper jwtHelper)
        {
            _customerClient = customerClient;
            _jwtHelper = jwtHelper;
        }

        public async Task<Response<AccessTokenResponse>> CreateToken(CreateTokenRequest request)
        {
            var customerResponse = await _customerClient.CustomerIsValid(request.CustomerId);
            
            if (!customerResponse.Success) 
                return new ErrorResponse<AccessTokenResponse>(ResponseStatus.BadRequest, default, ResultMessage.Error);
            
            var accessToken = _jwtHelper.GenereteJwtToken(request.CustomerId,request.Role);
            //var token = new TokenResponse {Token = JwtHelper.GenereteJwtToken(request, "Customer")};
            return new SuccessResponse<AccessTokenResponse>(accessToken);
        }

        public Response<TokenHandlerResponse> ValidateToken(string request)
        {
            var result = _jwtHelper.ValidateJwtToken(request);
            if (result.Status)
                return new SuccessResponse<TokenHandlerResponse>(result);
            return new ErrorResponse<TokenHandlerResponse>(ResponseStatus.UnAuthorized,result,ResultMessage.UnAuthorized);
        }
    }
}