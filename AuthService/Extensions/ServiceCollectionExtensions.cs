﻿using AuthService.Clients;
using AuthService.Helper;
using AuthService.Services;
using AuthService.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace AuthService.Extensions
{
    public static class ServiceCollectionExtensions
    {
        // public static IServiceCollection AddRepositories(this IServiceCollection services, IConfiguration configuration)
        // {
        //     return services;
        // }
        
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddSingleton<IAuthService, AuthServices>();
            services.AddSingleton<IJwtHelper, JwtHelper>();
            
            return services;
        }
        
        public static IServiceCollection AddClients(this IServiceCollection services)
        {
            services.AddSingleton<ICustomerClient,CustomerClient>();
            services.AddHttpClient();
            
            return services;
        }
        
        public static IServiceCollection AddUtilities(this IServiceCollection services)
        {

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "AuthService", Version = "v1" });
            });

            services.AddCors(options =>
            {
                options.AddPolicy("AllowMyOrigin", builder =>
                    builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader());
            });

            return services;
        }



    }
}