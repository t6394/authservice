﻿using System;
using System.Threading.Tasks;
using Core.ServerResponse;

namespace AuthService.Clients
{
    public interface ICustomerClient
    {
        Task<Response<bool>> CustomerIsValid(Guid id);
    }
}