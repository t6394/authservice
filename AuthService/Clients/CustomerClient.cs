﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using AuthService.Models.Dtos.Responses;
using Core.ServerResponse;
using Newtonsoft.Json;

namespace AuthService.Clients
{
    public class CustomerClient : ICustomerClient
    {
        private readonly HttpClient _httpClient;

        public CustomerClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<Response<bool>> CustomerIsValid(Guid id){
            HttpResponseMessage response = await _httpClient.GetAsync(ClientSettings.isValidateUrl + id);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<CustomerApiResponse>(responseBody);
                return new SuccessResponse<bool>(result.Data);
            }
            throw new InvalidOperationException("Customer validation error.");
        }

    }
}