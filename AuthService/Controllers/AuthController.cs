using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthService.Models.Dtos.Requests;
using AuthService.Models.Dtos.Responses;
using AuthService.Services.Interfaces;
using Core.Api;
using Core.ServerResponse;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AuthService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ApiController
    {
        private readonly IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        [HttpPost("/CreateToken")]
        public async Task<IActionResult> CreateToken([FromBody] CreateTokenRequest request)
        {
            var result = await _authService.CreateToken(request);
            return ApiResponse(result);
        }
        
        [HttpGet("/ValidateToken/{token}")]
        public IActionResult ValidateToken(string token)
        {
            var result = _authService.ValidateToken(token);
            return ApiResponse(result);
        }
    }
}